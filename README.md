## Contributing to qa_auto_playwright

### Husky, ESLint, and Prettier

We use a mix of [Husky](https://github.com/typicode/husky),
[ESLint](https://eslint.org/) and [Prettier](https://prettier.io/)
within our repository to help enforce consistent coding practices.
Husky is a tool that will install a pre-commit hook to run the linter any time before you attempt to make a commit.
This replaces the old pre-commit hook that was used before.
To install the pre-commit hook you will need to run

```bash
npm run prepare
```
